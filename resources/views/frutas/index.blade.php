
<h3>Listado de Frutas con foreach</h3>
{{-- Se utiliza helper action el cual crea URLs a la acción del controlador --}}
<a href="{{ route('peritas') }}">Ir a naranjas</a>
<a href="{{ action('FrutasController@anyPeras') }}" >Ir a peras </a>
@foreach($frutas as $fruta)
<br/>{{ "Nombre de la fruta ".$fruta }}
@endforeach

<h1>Formulario Laravel</h1>
<form action="{{ url('/recibir') }}" method="POST">
    <p>
        <label for="nombre">Nombre de la fruta</label>
        <input type="text" name="nombre" />
    </p>
    <p>
        <label for="descripcion">Descripción de la fruta</label>
        <input type="text" name="descripcion" />
    </p>
    <input type="submit" value="enviar" />
</form>