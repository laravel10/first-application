{{-- MI comentario en HTML --}}
<h1>Página de contacto de {{$nombre}} </h1>

{{-- Bucles --}}
<h3>Condicional If</h3>
@if(is_null($edad))
    No existe la edad
@else
    Su edad es {{$edad}}
@endif
<h3>Ciclo for</h3>
@for($i=1;$i < 10 ; $i++)
    <br/>{{$i.'X'.'2 ='.($i*2)}}
@endfor

<br/>
<h3>Ciclo while</h3>
<?php $f = 1 ?>
@while($f < 8)
    <br/>{{ "Bucle while ". $f }}
    <?php $f++ ?>
@endwhile
<h3>Listado de Frutas con foreach</h3>
@foreach($frutas as $fruta)
    <br/>{{ "Nombre de la fruta ".$fruta }}
@endforeach