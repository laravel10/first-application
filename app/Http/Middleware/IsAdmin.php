<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin {

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) {
    // El middleware se ejecutará antes de que se mande llamar el controller
    if(is_null($request->route('admin'))){
      // El redirect también se puede usar back() para mandar a la url anterior
      // y también se puede usar action paramencionar controller@action
      // llamando al método withInput() 
      return redirect()
              ->action('FrutasController@anyPeras')
              ->withInput(); // también se mandarán los datos por post al controller@action 
    }
    return $next($request);
  }

}
