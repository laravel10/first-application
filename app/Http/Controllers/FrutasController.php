<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class FrutasController extends Controller {

  /**
   * Se debe cambiar a la convención de camelCase para utilizar los métodos para
   * RESTful API: metodoNombreFuncion (get|post|delete|put|any)
   */
  public function getIndex() {
    $datos = array();
    $datos['frutas'] = array('naranja', 'pera', 'manzana', 'uva');
    // Se setean datos a la vista a través de un arreglo
    return view('frutas.index', $datos);
  }

  public function getNaranjas() {
    return 'Acción get de naranjas';
  }

  public function anyPeras($parametro = '') {
    return 'Acción para cualquier método (get|post|put|delete) de peras pasando '
            . 'N parámetros' . $parametro;
  }

  /**
   * Método que se manda ejecutar para recoger los datos del formulario
   * @param Request $request
   * @return string mensaje con nombre de la fruta que se recoge del formulario
   */
  public function recibirFormulario(Request $request) {
    return "El nombre de la fruta es: " . $request->input('nombre');
  }

}
