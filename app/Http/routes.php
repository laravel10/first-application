<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Los métodos pueden ser más de uno utilizando match[get,post], any, etc.
Route::get('/hola-mundo', function(){
    return json_encode([
        "json" => "Heil laravel"
    ]);
});
// Paso de parámetros opcionales con el símbolo ?
// Las validaciones se realizan con el método where y a través de expresiones
// regulares
Route::get('/contacto/{nombre?}/{edad?}', function($nombre = "",$edad = null){
    // Con array 
    /*
    return view('contacto',array(
        'nombre' => $nombre,
        'edad' => $edad
    ));
    */
    // POO
    return view('contacto.contacto')
            ->with('nombre',$nombre)
            ->with('edad',$edad)
            ->with('frutas',array('naranja','pera','manzana','uva'));
            

})-> where([
    'nombre' => '[A-Za-z]+', 
    'edad' => '[0-9]+',
]);
// Se agrega un prefijo que se antepondrá para todas las URL que se encuentren
// dentro del callback
Route::group(['prefix' => 'fruteria'],function(){
  // En lugar de pasar como segundo parámetro un callback/closure se pasa el nombre
  // del controlador y la acción
  Route::get('/frutas','FrutasController@getIndex');
  // Se indica que antes de que se ejecute el controller, se ejecutará el middleware
  // IsAdmin
  Route::get('/naranjas/{admin?}',[
                          'middleware' => 'IsAdmin',
                          'uses'       => 'FrutasController@getNaranjas'
  ]);
  Route::get('/peras',[
      'uses' =>'FrutasController@anyPeras',
      'as' =>'peritas', // Se crea un alias para llamar la ruta desde la vista
  ]);  
});
Route::post('/recibir','FrutasController@recibirFormulario');

// Si no se desea crear cada ruta por acción de controlador, se puede hacer:
//Route::controller('frutas','FrutasController');