<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFrutas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /*
        Schema::table('frutas',function(Blueprint $table){
          $table->string('pais')->after('nombre_fruta');
          $table->renameColumn('nombre_fruta','nombre');
        });
       */
      // Para ejecutar una consulta SQL en lugar de ocupar la sintaxis descrita 
      // en las líneas de arriba
      $create_table = (
              "create table probandosql(
                id int(255) auto_increment not null,
                publication int(255),
                primary key(id)
              )"
      );
      DB::statement($create_table);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
