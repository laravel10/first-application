/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  imondragon
 * Created: 11/12/2019
 */
create database laravelnotas character set utf8;

use laravelnotas;

create table notes(
id int(255) auto_increment not null,
title varchar(255),
decription text,
created_at datetime default null,
update_at datetime default null,
constraint pk_notes primary key(id)
);
